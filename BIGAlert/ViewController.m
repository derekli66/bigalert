
//  Created by LEE CHIEN-MING on 8/26/16.
//  Copyright © 2016 Derek Lee. All rights reserved.
//

#import "ViewController.h"
#import "BIGAlert.h"

@interface ViewController ()

@end

@implementation ViewController

- (IBAction)showAlert:(id)sender
{
    [BIGAlert alertWithTitle:@"Test"
                     message:@"Hello world!"
                      atMode:BIGAlertTypeNormal
              confirmedBlock:nil
         afterCancelledBlock:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
