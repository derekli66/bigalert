//  Created by LEE CHIEN-MING on 10/26/12.
//  Copyright (c) 2012 LEE CHIEN-MING. All rights reserved.
//

#import "BIGAlert.h"
#import "UIColor+Hex.h"

#define BIGALERT_BASIC_RECT CGRectMake(0.0f, 0.0f, 538.0f, 328.0f)
#define BUTTON_BASIC_RECT CGRectMake(0.0f, 0.0f, 188.0f, 45.0f)
#define CHECK_BUTTON_RECT CGRectMake(40.0f, 218.0f, 44.0f, 44.0f)
#define NOTICE_LABEL_RECT CGRectMake(86.0f, 216.0f, 400.0f, 44.0f)
#define TITLE_COLOR_HEX @"515151"
#define MESSAGE_COLOR_HEX @"a40000"
#define BUTTON_CENTER_Y 290.0f
#define SMALL_FONT_SIZE 26.0f

@interface BIGAlert()
@property (nonatomic) BIGAlertType currentAlertType;
/** The confirmation block is used while user presses the save or overwrite button
 */
@property (nonatomic, copy) void(^confirmationBlock)(void);

/** The cancellation block is used while user presses the cancel button.
 */
@property (nonatomic, copy) void(^cancellationBlock)(void);

/** The checkout block is used while user checks the "Do not show this window again".
 */
@property (nonatomic, copy) void(^checkoutBlock)(BOOL checked);

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) UIButton *overwriteButton;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UIButton *checkButton; //button for confirm "Do not show this window again."
@property (nonatomic, strong) UILabel *noticeLabel; //text message with "Do not show this window again."
@end

@implementation BIGAlert
#pragma mark - Initializaiton
-(void)createCheckButton
{
    _checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_checkButton setFrame:CHECK_BUTTON_RECT];
    [_checkButton setImage:[UIImage imageNamed:@"check0"] forState:UIControlStateNormal];
    [_checkButton setImage:[UIImage imageNamed:@"check1"] forState:UIControlStateHighlighted];
    [_checkButton setImage:[UIImage imageNamed:@"check1"] forState:UIControlStateSelected];
    [_checkButton addTarget:self action:@selector(checkForNeverShowed:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)createNoticeLabel
{
    _noticeLabel = [self createLabel];
    _noticeLabel.textAlignment = NSTextAlignmentLeft;
    [_noticeLabel setFont:[UIFont fontWithName:AlertFontType size:17.0f]];
    [_noticeLabel setText:@"Do not show this window again"];
    [_noticeLabel setFrame:NOTICE_LABEL_RECT];
    [_noticeLabel setTextColor:[UIColor colorWithWhite:0.45 alpha:1.0f]];
}

///This will setup all buttons according to different alert types///
-(void)setupButtonsWithType:(BIGAlertType)aType
{
    if (aType == BIGAlertTypeNormal) {
        _saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_saveButton setFrame:BUTTON_BASIC_RECT];
        [_saveButton setImage:[UIImage imageNamed:@"yes"] forState:UIControlStateNormal];
        [_saveButton setImage:[UIImage imageNamed:@"yes_push"] forState:UIControlStateHighlighted];
        [_saveButton addTarget:self action:@selector(runConfirmation:) forControlEvents:UIControlEventTouchUpInside];
        [self createNoticeLabel];
        [self createCheckButton];
    }
    else if (aType == BIGAlertTypeUseForOverwrite){
        _saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_saveButton setFrame:BUTTON_BASIC_RECT];
        [_saveButton setImage:[UIImage imageNamed:@"overwrite"] forState:UIControlStateNormal];
        [_saveButton setImage:[UIImage imageNamed:@"overwrite_push"] forState:UIControlStateHighlighted];
        [_saveButton addTarget:self action:@selector(runConfirmation:) forControlEvents:UIControlEventTouchUpInside];
        [self createNoticeLabel];
        [self createCheckButton];
    }
    else if (aType == BIGAlertTypeAlertOnly){
        _saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_saveButton setFrame:BUTTON_BASIC_RECT];
        [_saveButton setImage:[UIImage imageNamed:@"ok"] forState:UIControlStateNormal];
        [_saveButton setImage:[UIImage imageNamed:@"ok_push"] forState:UIControlStateHighlighted];
        [_saveButton addTarget:self action:@selector(runConfirmation:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    _cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_cancelButton setFrame:BUTTON_BASIC_RECT];
    [_cancelButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [_cancelButton setImage:[UIImage imageNamed:@"cancel_push"] forState:UIControlStateHighlighted];
    [_cancelButton addTarget:self action:@selector(runCancellation:) forControlEvents:UIControlEventTouchUpInside];
}

-(UILabel *)createLabel
{
    UILabel *label = [[UILabel alloc] init];
    [label setBackgroundColor:[UIColor clearColor]];
    label.frame = CGRectZero;
    label.textAlignment = NSTextAlignmentCenter;
    [label setFont:[UIFont fontWithName:AlertFontType size:34.0f]];
    return label;
}

///Setup the title label and message label///
-(void)setupLabelsWithTitle:(NSString *)aTit message:(NSString *)aMsg
{
    _titleLabel = [self createLabel];
    _messageLabel = [self createLabel];
    
    _titleLabel.frame = CGRectMake(0.0f, 5.0f, self.bounds.size.width, 90.0f);
    _messageLabel.frame = CGRectOffset(_titleLabel.frame, 0.0f, 82.0f); 
    UIEdgeInsets insets = UIEdgeInsetsMake(0.0f, 0.0f, -54.0f, 0.0f);
    _messageLabel.frame =  UIEdgeInsetsInsetRect(_messageLabel.frame, insets);
    _messageLabel.font = [UIFont fontWithName:AlertFontType size:44.0f];
    
    CGSize aSize = [aMsg sizeWithFont: [UIFont fontWithName:AlertFontType size:44.0]
                    constrainedToSize:CGSizeMake(_messageLabel.frame.size.width, 999.0f)
                        lineBreakMode:NSLineBreakByWordWrapping];
    
    if (aSize.height > _messageLabel.frame.size.height) {
        _messageLabel.font = [UIFont fontWithName:AlertFontType size:SMALL_FONT_SIZE];
        aSize = [aMsg sizeWithFont: [UIFont fontWithName:AlertFontType size:SMALL_FONT_SIZE]
                        constrainedToSize:CGSizeMake(_messageLabel.frame.size.width - 16.0f, 999.0f)
                            lineBreakMode:NSLineBreakByWordWrapping];
        
        _messageLabel.frame = CGRectMake(0.0f, _titleLabel.center.y + _titleLabel.frame.size.height/2 - 15.0f, aSize.width, aSize.height);
        _messageLabel.center = CGPointMake(_titleLabel.center.x, _messageLabel.center.y - 5.0f);
        _messageLabel.textAlignment = NSTextAlignmentLeft;
    }
    
    _titleLabel.textColor = [UIColor colorFromHexString:TITLE_COLOR_HEX];
    _messageLabel.textColor = [UIColor colorFromHexString:MESSAGE_COLOR_HEX];
    
    _titleLabel.numberOfLines = 0;
    _messageLabel.numberOfLines = 0;
    
    _titleLabel.text = aTit;
    _messageLabel.text = aMsg;
}

-(id)initWithTitle:(NSString *)str1 message:(NSString *)str2 atMode:(BIGAlertType)theType
{
    self = [super initWithFrame:BIGALERT_BASIC_RECT];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _currentAlertType = theType;
        if (_currentAlertType != BIGAlertTypeShowMessage) {
            [self setupButtonsWithType:_currentAlertType];
        }
        [self setupLabelsWithTitle:str1 message:str2];
        
        _backView = [[UIView alloc] initWithFrame:CGRectZero];
        _backView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.75];
    }
    return self;
}

#pragma mark - Custom Methods
+(void)alertWithTitle:(NSString *)aTitle message:(NSString *)aMsg atMode:(BIGAlertType)aType confirmedBlock:(void(^)(void))aSaved afterCancelledBlock:(void(^)(void))aCancelled
{
    [BIGAlert alertWithTitle:aTitle
                    message:aMsg
                     atMode:aType
               checkedBlock:nil
             confirmedBlock:aSaved
        afterCancelledBlock:aCancelled];
}

+(void)alertWithTitle:(NSString *)aTitle
              message:(NSString *)aMsg
               atMode:(BIGAlertType)aType
         checkedBlock:(void(^)(BOOL checked))aChecked
       confirmedBlock:(void (^)(void))aSaved
  afterCancelledBlock:(void (^)(void))aCancelled
{
    BIGAlert *alert = [[BIGAlert alloc] initWithTitle:aTitle message:aMsg atMode:aType];
    alert.confirmationBlock = aSaved;
    alert.cancellationBlock = aCancelled;
    alert.checkoutBlock = aChecked;
    [alert show];
}

#pragma mark - Private Methods
-(void)show
{
    UIView *aView = [[[[UIApplication sharedApplication] keyWindow] rootViewController] view];
    [aView addSubview:self];
    [self setCenter:CGPointMake(aView.bounds.size.width/2, aView.bounds.size.height/2)];

    self.hidden = NO;
    self.backView.alpha = 0.0f;
    self.backView.frame = self.superview.bounds;

    [UIView animateWithDuration:0.25f
                     animations:^{
                         self.transform = CGAffineTransformMakeScale(1.1, 1.1);
                         self.backView.alpha = 1.0f;
                     }
                     completion:^(BOOL finished) {
                         
                         if (finished) {
                             [UIView animateWithDuration:0.25f
                                              animations:^{
                                                  self.transform = CGAffineTransformIdentity;
                                              }
                                              completion:^(BOOL finished) {
                                                  if (finished) {
                                                      if (self.currentAlertType == BIGAlertTypeShowMessage) {
                                                          [self performSelector:@selector(runConfirmation:) withObject:nil afterDelay:1.8f];
                                                      }
                                                  }
                                              }];
                         }

                     }];
}

-(void)dismiss
{
    [UIView animateWithDuration:0.25f
                     animations:^{
                         self.transform = CGAffineTransformMakeScale(0.001, 0.001);
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             self.hidden = YES;
                             [self removeFromSuperview];
                             [self.backView removeFromSuperview];
                         }
                     }];
}

-(void)runConfirmation:(id)sender
{
    if (self.confirmationBlock) {
        self.confirmationBlock();
    }
    
    [self dismiss];
}

-(void)checkForNeverShowed:(UIButton *)btn
{
    btn.selected = !btn.selected;
    
    if (self.checkoutBlock) {
        self.checkoutBlock(btn.selected);
    }
}

-(void)runCancellation:(id)sender
{
    if (self.cancellationBlock) {
        self.cancellationBlock();
    }
    
    [self dismiss];
}

#pragma mark - UIKit Methods
-(void)willMoveToSuperview:(UIView *)newSuperview
{
    [super willMoveToSuperview:newSuperview];
    
    self.hidden = YES;
    self.transform = CGAffineTransformMakeScale(0.001, 0.001);
    
    [self addSubview:self.titleLabel];
    [self addSubview:self.messageLabel];
    
    if (self.currentAlertType == BIGAlertTypeAlertOnly) {
        self.saveButton.center = CGPointMake(280.0f, BUTTON_CENTER_Y);
        [self addSubview:self.saveButton];
    }
    else {
        self.saveButton.center = CGPointMake(140.0f, BUTTON_CENTER_Y);
        self.cancelButton.center = CGPointMake(400.0f, BUTTON_CENTER_Y);
        [self addSubview:self.saveButton];
        [self addSubview:self.cancelButton];
    }
    
    if (self.currentAlertType == BIGAlertTypeNormal || self.currentAlertType == BIGAlertTypeUseForOverwrite) {
        [self addSubview:self.checkButton];
        [self addSubview:self.noticeLabel];
    }
}

-(void)didMoveToSuperview
{
    [super didMoveToSuperview];
    [self.superview insertSubview:self.backView belowSubview:self];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[UIImage imageNamed:@"alert_bg"] drawInRect:self.bounds];
}


@end
