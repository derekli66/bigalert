//  Created by LEE CHIEN-MING on 10/26/12.
//  Copyright (c) 2012 LEE CHIEN-MING. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define AlertFontType @"HelveticaNeue-CondensedBold"

enum {
    BIGAlertTypeNormal = 0,
    BIGAlertTypeUseForOverwrite = 1,
    BIGAlertTypeAlertOnly = 2,
    BIGAlertTypeShowMessage = 3,
};
typedef NSUInteger BIGAlertType;

@interface BIGAlert : UIView
/** Used to pop up custom alert view. This alert view is used for cue saving and program saving.
 */
+(void)alertWithTitle:(NSString *)aTitle message:(NSString *)aMsg atMode:(BIGAlertType)aType confirmedBlock:(void(^)(void))aSaved afterCancelledBlock:(void(^)(void))aCancelled;

/** Used to pop up custom alert view with check button. This alert view is used for cue saving and program saving.
    This class method is only used for mode, BIGAlertTypeUseForOverwrite and BIGAlertTypeNormal
 */
+(void)alertWithTitle:(NSString *)aTitle
              message:(NSString *)aMsg
               atMode:(BIGAlertType)aType
         checkedBlock:(void(^)(BOOL checked))aChecked
       confirmedBlock:(void (^)(void))aSaved
  afterCancelledBlock:(void (^)(void))aCancelled;

@end
