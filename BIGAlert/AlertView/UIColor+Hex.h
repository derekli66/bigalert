//
//  Created by LEE CHIEN-MING on 8/26/16.
//  Copyright © 2016 Derek Lee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)
+ (UIColor *)colorFromHexString:(NSString *)hexString;
@end
